package learnreports;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReport {
	static ExtentHtmlReporter reporter;
	static ExtentReports extent;
	@BeforeSuite
	public void StartReport()
	{
	reporter = new ExtentHtmlReporter("./reports/results.html");  //path
	reporter.setAppendExisting(true); //To generate results for each run of same data
	extent =new ExtentReports();
	extent.attachReporter(reporter); //Attaching report
	}
	
	@Test
	public void report() throws IOException
	{
		ExtentTest test =  extent.createTest("MergeLead", "Create a new lead");//testName, description
		test.assignAuthor("Indu");
		test.assignCategory("Functional");
		test.pass("username entered successfully");
		test.fail("password not entered", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/snap1.png").build()); // ../ is the path of existing folder
	
	}
	
	@AfterSuite
	public void StopReport()
	{
	extent.flush();
	}
	
}