package learnExcel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	public static void main(String[] args) throws IOException {
		
		XSSFWorkbook wbook =new XSSFWorkbook("./Data/Excel Data.xlsx"); //string path
		XSSFSheet sheet = wbook.getSheetAt(1);
		int rowcount  = sheet.getLastRowNum();  //Right click , select surround with For loop
		System.out.println("Row count is:" +rowcount);
		int colcount = sheet.getRow(0).getLastCellNum(); //Right click , select surround with For loop
		System.out.println("Column count is:" +colcount);
		for (int i = 1; i <= rowcount; i++) {
			XSSFRow row = sheet.getRow(i);
		for (int j = 0; j < colcount; j++) {
			XSSFCell col = row.getCell(j);
			String stringCellValue = col.getStringCellValue();
			System.out.println(stringCellValue);
		}
		}
	}
}
		
		
	


