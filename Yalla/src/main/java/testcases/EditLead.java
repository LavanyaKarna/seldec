package testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class EditLead extends Annotation{
	 
		@BeforeTest(groups="sanity" , dependsOnGroups ="smoke")
		public void setData() {
			testcaseName= "EditLead";
			testcaseDec = "Edit the Lead in leaftaps";
			author      = "Indu";
			category    = "Smoke";
		}
		
		//@Test (dependsOnMethods = {"testcases.CreateLead.login"} ,alwaysRun=true)
		//@Test (dependsOnMethods = {"testcases.CreateLead.login2"} ,skipFailedInvocations=true)
		//@Test(groups = "sanity" , dependsOnGroups ="smoke")
		@Test(groups = "sanity")
		public void editLead() throws InterruptedException {
			click(locateElement("link", "CRM/SFA"));
			click(locateElement("link", "Leads"));
			click(locateElement("link", "Find Leads"));
			click(locateElement("xpath","//span[text()='Phone']"));
			clearAndType(locateElement("name", "phoneNumber"), "123"); 
		    click(locateElement("xpath","//button[text()='Find Leads']"));
		    Thread.sleep(1000);
		    click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		    click(locateElement("link", "Edit"));
		    clearAndType(locateElement("id","updateLeadForm_companyName"), "CTS");
		    click(locateElement("name", "submitButton"));
		}

	}





